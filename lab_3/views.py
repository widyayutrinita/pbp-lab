from django.shortcuts import render
from .forms import FriendForm
from django.contrib.auth.decorators import login_required, user_passes_test
from .models import Friend


# Create your views here.
@login_required(login_url='/admin/login/')
def index(request):
    friends = Friend.objects.all().values()
    response = {'friends': friends}
    return render(request, 'lab3_index.html', response)

@login_required
def add_friend(request):
    context = {}
    form = FriendForm(request.POST or None)
    if form.is_valid():
            form.save()
    
    context = {'form':form}
    return render(request, "lab3_form.html", context)
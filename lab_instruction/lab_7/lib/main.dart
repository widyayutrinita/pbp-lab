import 'package:flutter/material.dart';

void main(){
  runApp(MaterialApp(
    title: "Eduspace Registration",
    home : MyForm()
  ));
}

class MyForm extends StatefulWidget{
  @override
  _MyFormState createState() => _MyFormState();
}

class _MyFormState extends State<MyForm>{
  final _formKey = GlobalKey<FormState>();
  
  String name = "";
  String email= "";
  String password = "";
  
  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        title: const Text("Registration Form"),
        centerTitle: true
      ),
      body: Form(
        key: _formKey,
        child: SingleChildScrollView(
          child: Container(
            padding: const EdgeInsets.all(35.0),
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.all(7.0),
                  child: TextFormField(
                    decoration: const InputDecoration(
                      labelText: "Name",
                      hintText : "Enter Your Full Name (eg: Widya Ayu Trinita)",
                      icon: Icon(Icons.account_circle)
                    ),
                    validator: (String? value){
                      if(value == null){
                        return "Please enter your name";
                      }
                      name = value.toString();
                      return null;
                    }
                  )
                ),
                const SizedBox(
                  height: 10.0
                ),
                Padding(
                  padding: const EdgeInsets.all(7.0),
                  child: TextFormField(
                    decoration: const InputDecoration(
                      labelText: "Email",
                      hintText : "Enter Your Email (eg: Widyaat@gmail.com)",
                      icon: Icon(Icons.email)
                    ),
                    validator: (String? value){
                      if(value == null){
                        return "Please enter your email";
                      }
                      email = value.toString();
                      return null;
                    }
                  )
                ),
                Padding(
                  padding: const EdgeInsets.all(7.0),
                  child: TextFormField(
                    decoration: const InputDecoration(
                      labelText: "Password",
                      hintText : "Enter Your Password",
                      icon: Icon(Icons.lock)
                    ),
                    validator: (String? value){
                      if(value == null){
                        return "Please enter your password";
                      }
                      password = value.toString();
                      return null;
                    }
                  )
                ),
                const SizedBox(
                  height: 10.0
                ),
                ElevatedButton(
                  child: const Text("Register"),
                  onPressed: (){
                    if(_formKey.currentState!.validate()){
                      showDialog<String>(
                        context: context,
                        builder: (BuildContext context) => AlertDialog(
                          title: const Text("Done Submitted"),
                          content: Padding(
                              padding: const EdgeInsets.all(10.0),
                              child: Column(
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.all(15.0),
                                    child: Text("Your Name  : " + name)
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.all(15.0),
                                    child: Text("Your Email : " + email)
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.all(15.0),
                                    child: Text("Your Password : " + password)
                                  ),
                                  ]
                                ),
                              ),
                          actions: <Widget>[
                            ElevatedButton(
                              child: const Text("Okay"),
                              onPressed: () => Navigator.pop(context, "Okay")
                            )
                          ]
                        )
                      );
                    }
                  },
                  style: ElevatedButton.styleFrom(
                    primary: Colors.deepOrange,
                    onSurface: Colors.white12,
                  )
                )
              ]
            )
          )
        )
      )
    );
  }
}


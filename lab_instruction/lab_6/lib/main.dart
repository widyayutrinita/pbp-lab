import 'package:flutter/material.dart';

import 'screen/detail.dart';
import '.screen/home.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<Main> {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Eduspace',
      theme: ThemeData(
        scaffoldBackgroundColor: Colors.white, 
        fontFamily: 'Raleway',
      ),
      initialRoute: '/',
      routes: <String, WidgetBuilder> {
        '/': (context) => Home(),
        '/course': (context) => Detail(""),
      },
    );
  }
}

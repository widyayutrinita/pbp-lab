class Category {
  final String name;
  final String image;
  final String university;

  Category(this.name, this.university, this.image);
}

List<Category> categories = categoriesData
    .map((item) => Category(item['name'], item['courses'], item['image']))
    .toList();

var categoriesData = [
  {
    "name": "Economics",
    'courses': 'UI FEB',
    'image': "assets/images/waiting.png"
  },
  {
    "name": "UX Design",
    'courses': 'ITB',
    'image': "assets/images/waiting.png"
  },
  {
    "name": "DKV",
    'courses': 'UM',
    'image': "assets/images/waiting.png"
  },
  {
    "name": "Entrepreneurship", 
    'courses': 'ITB', 
    'image': "assets/images/waiting.png"},
  {
    "name": "Programming",
    'courses': 'ITB',
    'image': "assets/images/waiting.png"
  },
  {
    "name": "Math",
    'courses': 'UMM',
    'image': "assets/images/waiting.png"
  }
];

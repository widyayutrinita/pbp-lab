1. Apakah perbedaan antara JSON dan XML? 

JSON    : 
- JSON adalah Notasi Object JavaScript standar berbasis teks dalam pertukaran data
- Memiliki tipe bahasa meta
- Lebih sederhana
- File berakhiran .json
- Mendukung array
- Berorientasi pada data

XML     :
- XML merupakan bahasa markup extensible yang memiliki format independen perangkat lunak dan keras dalam pertukaran data
- Memiliki tipe bahasa markup
- Lebih rumit
- File berakhiran .xml
- Tidak mendukung array
- Berorientasi pada dokumen 

Yang paling membedakan adalah

2. Apakah perbedaan antara HTML dan XML?

HTML    :
- HTML atau HyperText Markup Language adalah bahasa pemrograman yang digunakan untuk membuat kerangka website
- Berfokus pada penyajian data
- Didorong oleh format
- Menitikberatkan pada struktur dan konteks
- Tidak menyediakan dukungan namespaces
- Strict terhadap tag penutup
- Tag ditentukan

XML     :
- XML atau eXtensible Markup Language 
- Berfokus pada transfer data
- Didorong oleh konten
- Menyediakan dukungan namespaces
- Tidak strict terhadap tag penutup
- Tag tidak ditentukan 

Referensi:
https://id.sawakinome.com/articles/technology/difference-between-json-and-xml-2.html 
https://blogs.masterweb.com/perbedaan-xml-dan-html/#Perbedaan_XML_dan_HTML 